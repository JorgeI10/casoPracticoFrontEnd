import React, { useEffect, useContext, Fragment } from 'react';
import AuthContext from '../../context/auth/authContext';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        color: '#E8570E'
    },
    title: {
        flexGrow: 1,
        fontWeight: 'bold',
        fontFamily: 'Monospace',
        letterSpacing: 5,
    },
    btn: {
        color: 'blue',
    },
}));

const Header = () => {
    const classes = useStyles();

    //extraer la información de autenticación
    const authContext = useContext(AuthContext);
    const { usuario, usuarioAutenticado, mostrar, cerrarSesion } = authContext;

    useEffect(() => {
        //se ejecuta cuando se recarga para no perder la información del usuario
        //cuando el token está en el localstorage
        if(localStorage.getItem('token')) usuarioAutenticado();
        // eslint-disable-next-line 
    }, []);

    return (
        <div className={classes.root}>
            <AppBar position="static" color="transparent">
                <Toolbar>
                    <Typography variant="h4" className={classes.title}>
                        Empresa X
                    </Typography>
                    {   (usuario)
                        ?   <Fragment>
                                <Button color="inherit" className={classes.btn} onClick={() => mostrar('Sucursales')}>
                                    Sucursales
                                </Button>
                                <Button color="inherit" className={classes.btn} onClick={() => mostrar('Productos')}>
                                    Productos
                                </Button>
                                <Button color="inherit" className={classes.btn} onClick={() => mostrar('Inventario')}>
                                    Inventario
                                </Button>
                                <Button color="inherit" className={classes.btn} onClick={() => mostrar('Ventas')}>
                                    Ventas
                                </Button>
                                <Button color="inherit" className={classes.btn} onClick={cerrarSesion}>
                                    Cerrar Sesion
                                </Button>
                            </Fragment>
                        :   null
                    }
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default Header;
