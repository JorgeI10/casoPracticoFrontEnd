import React, { useContext, useEffect } from 'react';
import Producto from './Producto';
import ProductoContext from '../../context/productos/productoContext';
import AlertaContext from '../../context/alertas/alertaContext';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

const ListadoProductos = () => {
    const classes = useStyles();

    //extrae productos de state inicial
    const productoContext = useContext(ProductoContext);
    const { mensaje, productos, obtenerProductos } = productoContext;
    
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;

    //Obtener productos cuando carga el componente
    useEffect(() => {
        //si hay un mensaje de error
        if(mensaje) mostrarAlerta(mensaje.msg, mensaje.categoria);

        obtenerProductos();
        // eslint-disable-next-line
    }, [mensaje]);

    //revisar si productos tiene contenido
    if(productos.length === 0) return <p>No hay productos</p>;

    return (
        <div className="contenedor-tabla">
            { (alerta) ? <div className={`alerta ${alerta.categoria}`}>{ alerta.msg }</div> : null }
            <TableContainer component={Paper}>
                <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Nombre</TableCell>
                        <TableCell>Código de Barras</TableCell>
                        <TableCell>Precio</TableCell>
                        <TableCell align="right"></TableCell>
                        <TableCell align="right"></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody >
                    {
                        productos.map(producto => (
                            <TableRow key={producto._id}>
                                <Producto  producto={producto}/>
                            </TableRow>
                        ))
                    }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}
 
export default ListadoProductos;
