import React,  { Fragment, useContext, useEffect, useState } from 'react';
import ProductoContext from '../../context/productos/productoContext';

const NuevoProducto = () => {

    //obtener el state del formulario
    const productoContext = useContext(ProductoContext);
    const { formulario, producto: productoSeleccionado, mostrarFormulario, agregarProducto, actualizarProducto } = productoContext;

    //state para nueva producto
    const [producto, guardarProducto] = useState({
        nombre: '',
        codigo: '',
        precio: ''
    });
    const [error, muestraError] = useState(false);

    const { nombre, codigo, precio } = producto;

    useEffect(() => {
        if(productoSeleccionado) {
            guardarProducto({
                ...producto,
                nombre: productoSeleccionado.nombre,
                codigo: productoSeleccionado.codigo,
                precio: productoSeleccionado.precio
            })
        } else {
            guardarProducto({
                ...producto,
                nombre: '',
                codigo: '',
                precio: ''
            })
        }
    // eslint-disable-next-line
    }, [productoSeleccionado]);

    const onChangeProducto = e => {
        guardarProducto({
            ...producto,
            [e.target.name]: e.target.value
        });
    };

    const onSubmitProducto = e => {
        e.preventDefault();
        //validar el producto
        if(nombre.trim() === '' || precio === '' || precio === 0 || codigo === '' || codigo === 0) {
            muestraError(true);
            return;
        }
        muestraError(false);
        
        //agregar al state
        if(productoSeleccionado) {
            productoSeleccionado.nombre = nombre;
            productoSeleccionado.codigo = codigo;
            productoSeleccionado.precio = precio;
            actualizarProducto(productoSeleccionado)
        } else {
            agregarProducto(producto);
        }
        //reiniciar form
        guardarProducto({ nombre: '', codigo: '', precio: '' });
    };

    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={() => mostrarFormulario()}
            >
                Nuevo Producto
            </button>
            {   (formulario || productoSeleccionado)
                ?   <form
                        className="formulario-nuevo-producto"
                        onSubmit={onSubmitProducto}
                    >
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Nombre Producto"
                            name="nombre"
                            value={nombre}
                            onChange={onChangeProducto}
                        />
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Código Barras"
                            name="codigo"
                            value={codigo}
                            onChange={onChangeProducto}
                        />
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Precio Producto"
                            name="precio"
                            value={precio}
                            onChange={onChangeProducto}
                        />
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value={`${productoSeleccionado ? 'Editar' : 'Agregar'} Producto`}
                        />
                    </form>
                :   null
            }
            {   (error)
                ?   <p className="mensaje error">Todos los campos son obligatorios y mayor a cero</p>
                :   null
            }
        </Fragment>
    );
}
 
export default NuevoProducto;