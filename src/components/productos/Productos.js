import React, { Fragment } from 'react';
import NuevoProducto from './NuevoProducto';
import ListadoProductos from './ListadoProductos';

const Productos = () => {
    return (
        <Fragment>
            <NuevoProducto />
            <div>
                <h2>Tus Productos</h2>
                <ListadoProductos />
            </div>
        </Fragment>
    );
}
 
export default Productos;