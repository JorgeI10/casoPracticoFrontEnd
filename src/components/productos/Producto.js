import React, { useContext, Fragment } from 'react';
import ProductolContext from '../../context/productos/productoContext';
import TableCell from '@material-ui/core/TableCell';

const Producto = ({ producto }) => {

    //obtener el state productos
    const productosContext = useContext(ProductolContext);
    const { productoActual, eliminarProducto } = productosContext;
    
    //Función para seleccionar producto
    const seleccionarProducto = id => {
        productoActual(id);
    };

    return (
        <Fragment>
            <TableCell>{producto.nombre}</TableCell>
            <TableCell>{producto.codigo}</TableCell>
            <TableCell>{`$ ${parseFloat(producto.precio).toFixed(2)}`}</TableCell>
            <TableCell align="right">
                <button
                    type="button"
                    className="btn btn-blank"
                    onClick={() => seleccionarProducto(producto._id)}
                >
                    Editar
                </button>
            </TableCell>
            <TableCell align="right">
                <button
                    type="button"
                    className="btn btn-blank"
                    onClick={() => eliminarProducto(producto._id)}
                >
                    Eliminar
                </button>
            </TableCell>
        </Fragment>
    );
}
 
export default Producto;