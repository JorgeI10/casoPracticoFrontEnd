import React, { useEffect, useContext, Fragment } from 'react';
import AuthContext from '../../context/auth/authContext';
import Sucursales from '../sucursales/Sucursales';
import Productos from '../productos/Productos';
import Inventarios from '../inventario/Inventarios';
import Ventas from '../ventas/Ventas';

const Principal = () => {
    
    //extraer la información de autenticación
    const authContext = useContext(AuthContext);
    const { mostrarElemento, usuarioAutenticado } = authContext;
    
    useEffect(() => {
        //se ejecuta cuando se recarga para no perder la información del usuario
        usuarioAutenticado();
    // eslint-disable-next-line 
    }, []);

    const elementoSeleccionado = () => {
        switch(mostrarElemento) {
            case 'Sucursales':
                return <Sucursales />;
            case 'Productos':
                return <Productos />;
            case 'Inventario':
                return <Inventarios />;
            case 'Ventas':
                return <Ventas />;
            default:
                return null;
        }
    };

    return (
        <Fragment>
            { elementoSeleccionado() }
        </Fragment>
    );
}
 
export default Principal;