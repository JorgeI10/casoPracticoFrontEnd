import React,  { Fragment, useContext, useEffect, useState } from 'react';
import InventarioContext from '../../context/inventarios/inventarioContext';

const ModificarInventario = () => {

    //obtener el state
    const inventarioContext = useContext(InventarioContext);
    const { inventario: inventarioSeleccionado, actualizarInventario, inventarioActual  } = inventarioContext;

    //state agregar stock a  inventario
    const [inventario, guardarInventario] = useState({
        cantidad: ''
    });
    const [error, muestraError] = useState(false);

    const { cantidad } = inventario;

    useEffect(() => {
        if(inventarioSeleccionado) {
            guardarInventario({
                ...inventario,
                nombre: inventarioSeleccionado.nombre,
                codigo: inventarioSeleccionado.codigo,
                precio: inventarioSeleccionado.precio
            })
        } else {
            guardarInventario({
                ...inventario,
                cantidad: ''
            });
        }
    // eslint-disable-next-line
    }, [inventarioSeleccionado]);

    const onChangeInventario = e => {
        guardarInventario({
            ...inventario,
            [e.target.name]: e.target.value
        });
    };

    const onSubmitInventario = e => {
        e.preventDefault();
        //validar el inventario
        if(cantidad === '' || cantidad === 0) {
            muestraError(true);
            return;
        }
        muestraError(false);
        
        //agregar al state
        inventarioSeleccionado.cantidad = cantidad;
        actualizarInventario(inventarioSeleccionado);

        //reiniciar form
        guardarInventario({
            ...inventario,
            cantidad: ''
        })
    };

    const cancelar = e => {
        e.preventDefault();
        //quitar el form 
        inventarioActual(null);
    };

    return (
        <Fragment>
            {   (inventarioSeleccionado)
                ?   <form
                        className="formulario-nuevo-producto"
                    >
                        <label htmlFor="cantidad">{ `${inventarioSeleccionado.sucursal.nombre} | ${inventarioSeleccionado.producto.nombre}:  ` } </label>
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Cantidad"
                            name="cantidad"
                            value={cantidad}
                            onChange={onChangeInventario}
                        />
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Agregar a Stock"
                            onClick={onSubmitInventario}
                        />
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Cancelar"
                            onClick={cancelar}
                        />
                    </form>
                :   null
            }
            {   (error)
                ?   <p className="mensaje error">Todos los campos son obligatorios y mayor a cero</p>
                :   null
            }
        </Fragment>
    );
}
 
export default ModificarInventario;