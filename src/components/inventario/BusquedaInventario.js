import React,  { Fragment, useContext, useEffect, useState } from 'react';
import InventarioContext from '../../context/inventarios/inventarioContext';

const BusquedaInventario = () => {

    //obtener el state del formulario
    const inventarioContext = useContext(InventarioContext);
    const { inventario, buscaProductoInventario } = inventarioContext;
   
    const [busqueda, guardaBusqueda] = useState('');

    useEffect(() => {
        //limpia el value cuando se selecciona un elemento
        if(inventario) guardaBusqueda('');
    }, [inventario])

    const onChangeProducto = e => {
        guardaBusqueda(e.target.value);
        buscaProductoInventario(e.target.value);
    };

    return (
        <Fragment>
            <div className="formulario-nuevo-producto">
                <input
                    type="text"
                    className="input-text"
                    placeholder="Busqueda Producto"
                    name="busqueda"
                    value={busqueda}
                    onChange={onChangeProducto}
                />
            </div>
        </Fragment>
    );
}
 
export default BusquedaInventario;