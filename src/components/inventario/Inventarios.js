import React, { Fragment } from 'react';
import BusquedaInventario from './BusquedaInventario';
import ModificarInventario from './ModificarInventario';
import ListadoInventarios from './ListadoInventarios';

const Inventarios = () => {
    return (
        <Fragment>
            <ModificarInventario />
            <BusquedaInventario />
            <div>
                <h2>Tu Inventario</h2>
                <ListadoInventarios />
            </div>
        </Fragment>
    );
}
 
export default Inventarios;