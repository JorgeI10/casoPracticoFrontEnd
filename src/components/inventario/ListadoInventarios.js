import React, { useContext, useEffect, useState, Fragment } from 'react';
import Inventario from './Inventario';
import InventarioContext from '../../context/inventarios/inventarioContext';
import SucursalContext from '../../context/sucursales/sucursalContext';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

const ListadoInventarios = () => {
    const classes = useStyles();

    //extrae inventarios de state inicial
    const inventarioContext = useContext(InventarioContext);
    const { mensaje, inventarios, productoBusqueda, obtenerInventarios } = inventarioContext;
    
    const sucursalContext = useContext(SucursalContext);
    const { sucursales, obtenerSucursales } = sucursalContext;

    //state para mostrar los ṕroductos
    const [productos, guardaProductos] = useState(inventarios);

    //Obtener inventarios cuando carga el componente
    useEffect(() => {

        obtenerSucursales();
        obtenerInventarios();
        // eslint-disable-next-line
    }, [mensaje]);

    //actualizar los inventarios 
    useEffect(() => {
        if(productoBusqueda.trim() === '') {
            guardaProductos(inventarios);
        }
        // eslint-disable-next-line
    }, [inventarios, productoBusqueda]);

    //actualizar las tablas cuando hay una busqueda
    useEffect(() => {
        if(productoBusqueda.trim() !== '') {
            const productosbusqueda = inventarios.filter(inventario =>
                inventario.producto.nombre.toLowerCase().includes(productoBusqueda.toLowerCase()
            ));
            guardaProductos(productosbusqueda);
        }
        obtenerInventarios();
        // eslint-disable-next-line
    }, [productoBusqueda]);

    //revisar si inventarios tiene contenido
    if(inventarios.length === 0) return <p>No hay productos</p>;

    return (
        <div className="contenedor-tabla">
            {   sucursales.map(sucursal => 
                    <Fragment key={sucursal._id}>
                        <h1>{ sucursal.nombre }</h1>
                        <TableContainer component={Paper}>
                            <Table className={classes.table} size="small" aria-label="a dense table">
                                <TableHead>
                                <TableRow>
                                    <TableCell>Nombre</TableCell>
                                    <TableCell>Código de Barras</TableCell>
                                    <TableCell>Cantidad</TableCell>
                                    <TableCell>Precio</TableCell>
                                    <TableCell align="right"></TableCell>
                                    <TableCell align="right"></TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody >
                                {
                                    productos.map(producto => (
                                        (producto.sucursal._id === sucursal._id) &&
                                        <TableRow key={producto._id}>
                                            <Inventario inventario={producto}/>
                                        </TableRow>
                                    ))
                                }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Fragment>
                )
            }
        </div>
    );
}
 
export default ListadoInventarios;
