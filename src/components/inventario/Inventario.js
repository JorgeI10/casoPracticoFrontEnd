import React, { useContext, Fragment } from 'react';
import InventarioContext from '../../context/inventarios/inventarioContext';
import TableCell from '@material-ui/core/TableCell';

const Inventario = ({ inventario }) => {

    //obtener el state inventarios
    const inventarioContext = useContext(InventarioContext);
    const { inventarioActual } = inventarioContext;
    
    //Función para seleccionar inventario
    const seleccionarInventario = id => {
        inventarioActual(id);
    };

    return (
        <Fragment>
            {   inventario && 
                <Fragment>
                    <TableCell>{inventario.producto.nombre}</TableCell>
                    <TableCell>{inventario.producto.codigo}</TableCell>
                    <TableCell>{inventario.cantidad}</TableCell>
                    <TableCell>{`$ ${parseFloat(inventario.producto.precio).toFixed(2)}`}</TableCell>
                    <TableCell align="right">
                        <button
                            type="button"
                            className="btn btn-blank"
                            onClick={() => seleccionarInventario(inventario._id)}
                        >
                            Agregar Stock
                        </button>
                    </TableCell>
                </Fragment>
            }
        </Fragment>
    );
}
 
export default Inventario;