import React, { Fragment } from 'react';
import NuevaSucursal from '../sucursales/NuevaSucursal';
import ListadoSucursales from '../sucursales/ListadoSucursales';

const Sucursales = () => {
    return (
        <Fragment>
            <NuevaSucursal />
            <div>
                <h2>Tus Sucursales</h2>
                <ListadoSucursales />
            </div>
        </Fragment>
    );
}
 
export default Sucursales;