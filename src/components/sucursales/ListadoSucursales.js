import React, { useContext, useEffect } from 'react';
import Sucursal from './Sucursal';
import SucursalContext from '../../context/sucursales/sucursalContext';
import AlertaContext from '../../context/alertas/alertaContext';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

const ListadoSucursales = () => {
    const classes = useStyles();

    //extrae sucursales de state inicial
    const sucursalContext = useContext(SucursalContext);
    const { mensaje, sucursales, obtenerSucursales } = sucursalContext;
    
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;

    //Obtener sucursales cuando carga el componente
    useEffect(() => {
        //si hay un mensaje de error
        if(mensaje) mostrarAlerta(mensaje.msg, mensaje.categoria);

        obtenerSucursales();
        // eslint-disable-next-line
    }, [mensaje]);

    //revisar si sucursales tiene contenido
    if(sucursales.length === 0) return <p>No hay sucursales</p>;

    return (
        <div className="contenedor-tabla">
            { (alerta) ? <div className={`alerta ${alerta.categoria}`}>{ alerta.msg }</div> : null }
            <TableContainer component={Paper}>
                <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                    <TableRow>
                        <TableCell>Nombre</TableCell>
                        <TableCell align="right"></TableCell>
                        <TableCell align="right"></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody >
                    {
                        sucursales.map(sucursal => (
                            <TableRow key={sucursal._id}>
                                <Sucursal  sucursal={sucursal}/>
                            </TableRow>
                        ))
                    }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}
 
export default ListadoSucursales;
