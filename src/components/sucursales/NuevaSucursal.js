import React,  { Fragment, useContext, useEffect, useState } from 'react';
import SucursalContext from '../../context/sucursales/sucursalContext';

const NuevaSucursal = () => {

    //obtener el state del formulario
    const sucursalContext = useContext(SucursalContext);
    const { formulario, sucursal: sucursalSeleccionada, mostrarFormulario, agregarSucursal, actualizarSucursal } = sucursalContext;

    //state para nueva sucursal
    const [sucursal, guardarSucursal] = useState({
        nombre: ''
    });
    const [error, muestraError] = useState(false);

    const { nombre } = sucursal;

    useEffect(() => {
        if(sucursalSeleccionada) {
            guardarSucursal({
                ...sucursal,
                nombre: sucursalSeleccionada.nombre
            })
        } else {
            guardarSucursal({
                ...sucursal,
                nombre: ''
            })
        }
    // eslint-disable-next-line
    }, [sucursalSeleccionada]);

    const onChangeSucursal = e => {
        guardarSucursal({
            ...sucursal,
            [e.target.name]: e.target.value
        });
    };

    const onSubmitSucursal = e => {
        e.preventDefault();
        //validar el sucursal
        if(nombre.trim() === '') {
            muestraError(true);
            return;
        }
        muestraError(false);
        
        //agregar al state
        if(sucursalSeleccionada) {
            sucursalSeleccionada.nombre = nombre;
            actualizarSucursal(sucursalSeleccionada)
        } else {
            agregarSucursal(sucursal);
        }
        //reiniciar form
        guardarSucursal({ nombre: '' });
    };

    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={() => mostrarFormulario()}
            >
                Nueva Sucursal
            </button>
            {   (formulario || sucursalSeleccionada)
                ?   <form
                        className="formulario-nueva-sucursal"
                        onSubmit={onSubmitSucursal}
                    >
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Nombre Sucursal"
                            name="nombre"
                            value={nombre}
                            onChange={onChangeSucursal}
                        />
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value={`${sucursalSeleccionada ? 'Editar' : 'Agregar'} Sucursal`}
                        />
                    </form>
                :   null
            }
            {   (error)
                ?   <p className="mensaje error"> El nombre de la sucursal es obligatorio</p>
                :   null
            }
        </Fragment>
    );
}
 
export default NuevaSucursal;