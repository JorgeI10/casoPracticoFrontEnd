import React, { useContext, Fragment } from 'react';
import SucursalContext from '../../context/sucursales/sucursalContext';
import TableCell from '@material-ui/core/TableCell';

const Sucursal = ({ sucursal }) => {

    //obtener el state sucursales
    const sucursalesContext = useContext(SucursalContext);
    const { sucursalActual, eliminarSucursal } = sucursalesContext;
    
    //Función para seleccionar sucursal
    const seleccionarSucursal = id => {
        sucursalActual(id);
    };

    return (
        <Fragment>
            <TableCell>{sucursal.nombre}</TableCell>
            <TableCell align="right">
                <button
                    type="button"
                    className="btn btn-blank"
                    onClick={() => seleccionarSucursal(sucursal._id)}
                >
                    Editar
                </button>
            </TableCell>
            <TableCell align="right">
                <button
                    type="button"
                    className="btn btn-blank"
                    onClick={() => eliminarSucursal(sucursal._id)}
                >
                    Eliminar
                </button>
            </TableCell>
        </Fragment>
    );
}
 
export default Sucursal;