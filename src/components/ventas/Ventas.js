import React, { Fragment, useState } from 'react';
import FormularioVentas from './FormularioVentas';
import TablaVentas from './TablaVentas';

const Ventas = () => {
    
    const [elementosCompra, guardaElementosCompra] = useState([]);
    const [total, guardaTotal] = useState(0);

    return (
        <Fragment>
            <FormularioVentas
                elementosCompra={elementosCompra}
                guardaElementosCompra={guardaElementosCompra}
                total={total}
                guardaTotal={guardaTotal}
            />
            <TablaVentas
                elementosCompra={elementosCompra}
                guardaElementosCompra={guardaElementosCompra}
                total={total}
            />
        </Fragment>
    );
}
 
export default Ventas;