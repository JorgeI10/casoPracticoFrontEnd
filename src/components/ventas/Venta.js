import React, { Fragment } from 'react';
import TableCell from '@material-ui/core/TableCell';

const Venta = ({ venta }) => {

    return (
        <Fragment>
            <TableCell>{ venta.sucursal.nombre }</TableCell>
            <TableCell>{ venta.producto.nombre }</TableCell>
            <TableCell>{ venta.cantidad }</TableCell>
            <TableCell>{ `$ ${parseFloat(venta.producto.precio).toFixed(2) }` }</TableCell>
            <TableCell>{ `$ ${parseFloat(venta.cantidad * venta.producto.precio).toFixed(2) }` }</TableCell>
        </Fragment>
    );
}
 
export default Venta;