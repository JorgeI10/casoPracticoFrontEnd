import React,  { Fragment, useContext, useEffect, useState } from 'react';
import SucursalContext from '../../context/sucursales/sucursalContext';
import ProductoContext from '../../context/productos/productoContext';
import InventarioContext from '../../context/inventarios/inventarioContext';

const FormularioVentas = ({ elementosCompra, guardaElementosCompra, total, guardaTotal }) => {

    //obtener el state
    const sucursalContext = useContext(SucursalContext);
    const { sucursales, obtenerSucursales } = sucursalContext;

    const productoContext = useContext(ProductoContext);
    const { productos, obtenerProductos } = productoContext;

    const inventarioContext = useContext(InventarioContext);
    const { cerrarVenta } = inventarioContext;
    
    //state para ventas
    const [seleccionado, guardarSeleccionado] = useState({
        sucursal: '',
        producto: '',
        cantidad: ''
    });
    const [error, muestraError] = useState(false);
    
    const { sucursal, producto, cantidad } = seleccionado;

    useEffect(() => {
        //obtener sucursales y productos
        obtenerSucursales();
        obtenerProductos();
        // eslint-disable-next-line
    }, []);

    const onChangeSucursal = e => {
        guardarSeleccionado({
            ...seleccionado,
            [e.target.name]: e.target.value
        });
    };

    const agregar = e => {
        e.preventDefault();
        // eslint-disable-next-line
        const valueSucursal = sucursales.find(suc => suc._id === sucursal);
        // eslint-disable-next-line
        const valueProducto = productos.find(prd => prd._id === producto);

        if(valueSucursal && valueProducto && cantidad > 0 && cantidad !== 0 && parseInt(cantidad)) {
            const totalVenta = parseInt(total) + parseInt(cantidad * valueProducto.precio);
            guardaTotal(totalVenta);
            
            guardaElementosCompra([
                ...elementosCompra,
                { sucursal: valueSucursal, producto: valueProducto, cantidad }
            ]);
            muestraError(false);
        } else {
            muestraError(true);
        }

        guardarSeleccionado({ sucursal: '', producto: '', cantidad: '' });
    };
    
    const onSubmitCerrarVenta = e => {
        e.preventDefault();
        guardarSeleccionado({ sucursal: '', producto: '', cantidad: '' });

        cerrarVenta(elementosCompra);
        guardaElementosCompra([]);
        guardaTotal(0);
    };

    return (
        <Fragment>
            <form className="formulario-nueva-sucursal contenedor-tabla">
                <select
                    className="input-text"
                    name="sucursal"
                    value={sucursal}
                    onChange={onChangeSucursal}
                >
                    <option value="">- Sucursal -</option>
                    {   sucursales.map(sucursal => (
                            <option key={sucursal.nombre} value={sucursal._id}>{ sucursal.nombre }</option>
                        )) 
                    }
                </select>
                <select
                    className="input-text"
                    name="producto"
                    value={producto}
                    onChange={onChangeSucursal}
                >
                    <option value="">- Producto -</option>
                    {   productos.map(producto => (
                            <option key={producto._id} value={producto._id}>{ producto.nombre }</option>
                        )) 
                    }
                </select>
                <input
                    type="text"
                    className="input-text"
                    name="cantidad"
                    placeholder="Cantidad"
                    value={cantidad}
                    onChange={onChangeSucursal}
                />
                <input
                    type="submit"
                    className="btn btn-primario btn-block"
                    value='Agregar'
                    onClick={agregar}
                />
                <input
                    type="submit"
                    className="btn btn-primario btn-block"
                    value='Cerrar Venta'
                    onClick={onSubmitCerrarVenta}
                />
            {   (error)
                ?   <p className="mensaje error">Todos los campos son obligatorios</p>
                :   null
            }
            </form>
        </Fragment>
    );
}
 
export default FormularioVentas;