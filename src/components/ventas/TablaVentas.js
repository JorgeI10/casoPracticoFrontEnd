import React from 'react';
import Venta from './Venta';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

const TablaVentas = ({ elementosCompra, guardaElementosCompra, total }) => {
    const classes = useStyles();

    return (
        <div className="contenedor-tabla tabla-ventas">
            {   (elementosCompra.length > 0)
                ?   <TableContainer component={Paper}>
                        <Table className={classes.table} size="small" aria-label="a dense table">
                            <TableHead>
                            <TableRow>
                                <TableCell>Sucursal</TableCell>
                                <TableCell>Producto</TableCell>
                                <TableCell>Cantidad</TableCell>
                                <TableCell>Precio</TableCell>
                                <TableCell>Total</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody >
                            {
                                elementosCompra.map((venta, i) => (
                                    <TableRow key={i}>
                                        <Venta venta={venta}/>
                                    </TableRow>
                                ))
                            }
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell></TableCell>
                                <TableCell></TableCell>
                                <TableCell></TableCell>
                                <TableCell>{ `$ ${parseFloat(total).toFixed(2)}` }</TableCell>
                            </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                :   null
            }
        </div>
    );
}
 
export default TablaVentas;