import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/auth/authContext';

const CrearCuenta = (props) => {
    //extraer los valores de context alerta
    const alertaContext = useContext(AlertaContext);   
    const { alerta, mostrarAlerta } = alertaContext; 
     
    //extraer los valores de context auth
    const authContext = useContext(AuthContext);   
    const { mensaje, autenticado, registrarUsuario } = authContext; 
 
    //en caso de que el usuario se haya autenticado o registrado o sea un registro duplicado
    useEffect(() => {
        if(autenticado) {
            props.history.push('/principal');
        }
 
        if(mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria);
        }
    // eslint-disable-next-line 
    }, [mensaje, autenticado, props.history]);
 
    //state para iniciar sesión
    const [usuario, guardarUsuario] = useState({
        nombre: '',
        email: '',
        password: '',
        confirmar: ''
    }); 
 
    //extraer de state usuario
    const { nombre, email, password, confirmar } = usuario;
 
    const onChange = e => {
        guardarUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    };
 
    //registra e inicia sesión
    const onSubmit = e => {
        e.preventDefault();
 
        //validar campos vacios
        if(nombre.trim() === '' || email.trim() === '' || password.trim() === '' || confirmar.trim() === '') {
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
            return;
        }

        //Password mínimo de 6 caracteres
        if(password.trim().length < 6) {
            mostrarAlerta('El password debe ser de, al menos, 6 caracteres', 'alerta-error');
            return;
        } 
 
        //Los dos password son iguales
        if(password.trim() !== confirmar.trim()) {
            mostrarAlerta('Los password no coinciden', 'alerta-error');
            return;
        } 
 
        //pasarlo al action
        registrarUsuario({ nombre, email, password });
    };
 
    return (
        <div className="form-usuario">
            { alerta ? <div className={`alerta ${alerta.categoria}`}>{ alerta.msg }</div> : null }
            <div className="contenedor-form sombra-dark">
                <h1>Crea una Nueva Cuenta</h1>
                <form onSubmit={onSubmit}>
                    <div className="campo-form">
                        <label htmlFor="email">Nombre</label>
                        <input
                            id="nombre"
                            type="text"
                            name="nombre"
                            placeholder="Tu nombre"
                            onChange={onChange}
                            value={nombre}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            id="email"
                            type="email"
                            name="email"
                            placeholder="Tu email"
                            onChange={onChange}
                            value={email}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            id="password"
                            type="password"
                            name="password"
                            placeholder="Tu password"
                            onChange={onChange}
                            value={password}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="confirmar">Confirmar Password</label>
                        <input
                            id="confirmar"
                            type="password"
                            name="confirmar"
                            placeholder="Confirma tu password"
                            onChange={onChange}
                            value={confirmar}
                        />
                    </div>
                    <div className="campo-form">
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Registrar Cuenta"
                        />
                    </div>
                </form>
                <Link to="/" className="enlace-cuenta">
                    Iniciar Sesión
                </Link>
            </div>
        </div>
    );
}
 
export default CrearCuenta;