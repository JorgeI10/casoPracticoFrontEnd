import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/auth/authContext';

const Login = (props) => {

    //extraer los valores de context alerta
    const alertaContext = useContext(AlertaContext);   
    const { alerta, mostrarAlerta } = alertaContext; 
            
    //extraer los valores de context auth
    const authContext = useContext(AuthContext);   
    const { mensaje, autenticado, iniciarSesion } = authContext; 
    
    //state para iniciar sesión
    const [usuario, guardarUsuario] = useState({
        email: '',
        password: ''
    }); 
    
    //extraer de state usuario
    const { email, password } = usuario;
    
    //en caso de que el password o usuario no existan
    useEffect(() => {
        if(autenticado) {
            props.history.push('/principal');
        }
    
        if(mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria);
        }
     // eslint-disable-next-line
    }, [mensaje, autenticado, props.history]);
    
    const onChange = e => {
        guardarUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    };
    
    //inicia sesión
    const onSubmit = e => {
        e.preventDefault();
        //validar campos vaciós
        if(email.trim() === '' || password.trim() === '') {
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error')
            return;
        }
        //pasarlo al action
        iniciarSesion({ email, password });
     }; 

    return (
        <div className="form-usuario">
            { alerta ? <div className={`alerta ${alerta.categoria}`}>{ alerta.msg }</div> : null }
            <div className="contenedor-form sombra-dark">
                <h1>Iniciar Sesión</h1>
                <form onSubmit={onSubmit} noValidate>
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            id="email"
                            type="email"
                            name="email"
                            placeholder="Tu email"
                            onChange={onChange}
                            value={email}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            id="password"
                            type="password"
                            name="password"
                            placeholder="Tu password"
                            onChange={onChange}
                            value={password}
                        />
                    </div>
                    <div className="campo-form">
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Iniciar Sesión"
                        />
                    </div>
                </form>
                <Link to="/crear-cuenta" className="enlace-cuenta">
                    Crear Cuenta
                </Link>
            </div>
        </div>
    );
}
 
export default Login;