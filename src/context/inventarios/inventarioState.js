import React, { useReducer } from 'react';
import inventarioContext from './inventarioContext';
import inventarioReducer from './inventarioReducer';
import clienteAxios from '../../config/axios';
import {
    OBTENER_INVENTARIOS, INVENTARIO_ACTUAL, ACTUALIZAR_INVENTARIO, INVENTARIO_ERROR, INVENTARIO_BUSQUEDA
} from '../../types';
 
const InventariosState = props => {

    const initialState = {
        inventarios: [],
        inventario: null,
        productoBusqueda: '',
        mensaje: null
    };

    //Dispatch para ejecutar acciones
    const [state, dispatch] = useReducer(inventarioReducer, initialState)

    //Obtener los inventarios
    const obtenerInventarios = async () => {
        try {
            const resultado = await clienteAxios.get('/api/inventarios');
            dispatch({
                type: OBTENER_INVENTARIOS,
                payload: resultado.data.inventarios
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: INVENTARIO_ERROR,
                payload: alerta
            });
        }
    };

    //agrega el texto del campo de busqueda de producto en inventarios
    const buscaProductoInventario = texto => {
        dispatch({
            type: INVENTARIO_BUSQUEDA,
            payload: texto
        });
    };

    //selecciona el inventario que el usuario da click
    const inventarioActual = inventarioID => {
        dispatch({
            type: INVENTARIO_ACTUAL,
            payload: inventarioID
        });
    };

    //edita o modifica la inventario
    const actualizarInventario = async inventario => {
        try {
            const resultado = await clienteAxios.put(`/api/inventarios/${inventario._id}`, inventario);

            dispatch({
                type: ACTUALIZAR_INVENTARIO,
                payload: resultado.data.inventario
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: INVENTARIO_ERROR,
                payload: alerta
            });
        }
    };

    const cerrarVenta = async productos => {
        try {
            const arrayProductos = productos.map( producto => ({
                cantidad: producto.cantidad, 
                sucursal: producto.sucursal._id,
                producto: producto.producto._id
            }));
            const resultado = await clienteAxios.post(`/api/inventarios/venta`, { productos: arrayProductos});

            dispatch({
                type: OBTENER_INVENTARIOS,
                payload: resultado.data.inventario
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: INVENTARIO_ERROR,
                payload: alerta
            });
        }
    };

    return (
        <inventarioContext.Provider
            value={{
                inventarios: state.inventarios,
                inventario: state.inventario,
                productoBusqueda: state.productoBusqueda,
                mensaje: state.mensaje,
                obtenerInventarios,
                inventarioActual,
                actualizarInventario,
                buscaProductoInventario,
                cerrarVenta
            }}
        >
            { props.children }
        </inventarioContext.Provider>
    )
}

export default InventariosState;
