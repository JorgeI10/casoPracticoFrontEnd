import {
    OBTENER_INVENTARIOS, INVENTARIO_ACTUAL, ACTUALIZAR_INVENTARIO, INVENTARIO_ERROR, INVENTARIO_BUSQUEDA
} from '../../types';

// eslint-disable-next-line 
export default (state, action) => {
    switch (action.type) {
        case INVENTARIO_BUSQUEDA:
            return {
                ...state,
                productoBusqueda: action.payload,
            }
        case OBTENER_INVENTARIOS:
            return {
                ...state,
                inventarios: action.payload
            };
        case INVENTARIO_ACTUAL:
            return {
                ...state,
                inventario: state.inventarios.find(inventario => inventario._id === action.payload),
                productoBusqueda: ''
            };
        case ACTUALIZAR_INVENTARIO:
            return {
                ...state,
                inventarios: state.inventarios.map(inventario => 
                        inventario._id === action.payload._id
                        ?   { ...inventario,
                                cantidad: action.payload.cantidad
                            }
                        : inventario
                    ),
                inventario: null
            }
        case INVENTARIO_ERROR:
            return {
                ...state,
                mensaje: action.payload
            }
        default:
            return state;
    }
}