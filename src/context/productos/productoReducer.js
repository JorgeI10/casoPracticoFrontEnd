import {
    AGREGAR_PRODUCTO, FORMULARIO_PRODUCTO, OBTENER_PRODUCTOS, PRODUCTO_ACTUAL, ACTUALIZAR_PRODUCTO, ELIMINAR_PRODUCTO, PRODUCTO_ERROR
} from '../../types';

// eslint-disable-next-line 
export default (state, action) => {
    switch (action.type) {
        case FORMULARIO_PRODUCTO:
            return {
                ...state,
                formulario: !state.formulario,
                producto: null
            };
        case OBTENER_PRODUCTOS:
            return {
                ...state,
                productos: action.payload,
                producto: null
            };
        case AGREGAR_PRODUCTO:
            return {
                ...state,
                productos: [
                    ...state.productos,
                    action.payload
                ],
                formulario: false,
                producto: null
            };
        case PRODUCTO_ACTUAL:
            return {
                ...state,
                producto: state.productos.find(producto => producto._id === action.payload),
                formulario: false
            };
        case ACTUALIZAR_PRODUCTO:
            return {
                ...state,
                productos: state.productos.map(producto => producto._id === action.payload._id ? action.payload : producto),
                producto: null
            }
        case ELIMINAR_PRODUCTO:
            return {
                ...state,
                productos: state.productos.filter(producto => producto._id !== action.payload),
                producto: null
            };
        case PRODUCTO_ERROR:
            return {
                ...state,
                mensaje: action.payload
            }
        default:
            return state;
    }
}