import React, { useReducer } from 'react';
import productoContext from './productoContext';
import productoReducer from './productoReducer';
import clienteAxios from '../../config/axios';
import {
    AGREGAR_PRODUCTO, FORMULARIO_PRODUCTO, OBTENER_PRODUCTOS, PRODUCTO_ACTUAL, ACTUALIZAR_PRODUCTO, ELIMINAR_PRODUCTO, PRODUCTO_ERROR
} from '../../types';
 
const ProductoState = props => {

    const initialState = {
        formulario: false,
        productos: [],
        producto: null,
        mensaje: null
    };

    //Dispatch para ejecutar acciones
    const [state, dispatch] = useReducer(productoReducer, initialState)

    //serie de funciones para CRUD
    const mostrarFormulario = () => {
        dispatch({
            type: FORMULARIO_PRODUCTO
        });
    };

    //Obtner los productos
    const obtenerProductos = async () => {
        try {
            const resultado = await clienteAxios.get('/api/productos');

            dispatch({
                type: OBTENER_PRODUCTOS,
                payload: resultado.data.productos
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: PRODUCTO_ERROR,
                payload: alerta
            });
        }
    };

    //Agregar producto
    const agregarProducto = async producto => {
        try {
            const resultado = await clienteAxios.post('/api/productos', producto);
            dispatch({
                type: AGREGAR_PRODUCTO,
                payload: resultado.data
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: PRODUCTO_ERROR,
                payload: alerta
            });
        }
    };

    //selecciona el producto que el usuario da click
    const productoActual = productoID => {
        dispatch({
            type: PRODUCTO_ACTUAL,
            payload: productoID
        });
    };

    //edita o modifica la producto
    const actualizarProducto = async producto => {
        try {
            const resultado = await clienteAxios.put(`/api/productos/${producto._id}`, producto);

            dispatch({
                type: ACTUALIZAR_PRODUCTO,
                payload: resultado.data.producto
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: PRODUCTO_ERROR,
                payload: alerta
            });
        }
    };

    //elimina producto
    const eliminarProducto = async productoID => {
        try {
            await clienteAxios.delete(`/api/productos/${productoID}`);
            dispatch({
                type: ELIMINAR_PRODUCTO,
                payload: productoID
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: PRODUCTO_ERROR,
                payload: alerta
            });
        }
    };

    return (
        <productoContext.Provider
            value={{
                formulario: state.formulario,
                productos: state.productos,
                producto: state.producto,
                mensaje: state.mensaje,
                mostrarFormulario,
                obtenerProductos,
                agregarProducto,
                productoActual,
                actualizarProducto,
                eliminarProducto
            }}
        >
            {props.children}
        </productoContext.Provider>
    )
}

export default ProductoState;
