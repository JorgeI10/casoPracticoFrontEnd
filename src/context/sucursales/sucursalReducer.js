import {
    AGREGAR_SUCURSAL, FORMULARIO_SUCURSAL, OBTENER_SUCURSALES, SUCURSAL_ACTUAL, ACTUALIZAR_SUCURSAL, ELIMINAR_SUCURSAL, SUCURSAL_ERROR
} from '../../types';

// eslint-disable-next-line 
export default (state, action) => {
    switch (action.type) {
        case FORMULARIO_SUCURSAL:
            return {
                ...state,
                formulario: !state.formulario,
                sucursal: null
            };
        case OBTENER_SUCURSALES:
            return {
                ...state,
                sucursales: action.payload,
                sucursal: null
            };
        case AGREGAR_SUCURSAL:
            return {
                ...state,
                sucursales: [
                    ...state.sucursales,
                    action.payload
                ],
                formulario: false,
                sucursal: null
            };
        case SUCURSAL_ACTUAL:
            return {
                ...state,
                sucursal: state.sucursales.find(sucursal => sucursal._id === action.payload),
                formulario: false
            };
        case ACTUALIZAR_SUCURSAL:
            return {
                ...state,
                sucursales: state.sucursales.map(sucursal => sucursal._id === action.payload._id ? action.payload : sucursal),
                sucursal: null
            }
        case ELIMINAR_SUCURSAL:
            return {
                ...state,
                sucursales: state.sucursales.filter(sucursal => sucursal._id !== action.payload),
                sucursal: null
            };
        case SUCURSAL_ERROR:
            return {
                ...state,
                mensaje: action.payload
            }
        default:
            return state;
    }
}