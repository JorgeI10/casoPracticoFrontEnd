import React, { useReducer } from 'react';
import sucursalContext from './sucursalContext';
import sucursalReducer from './sucursalReducer';
import clienteAxios from '../../config/axios';
import {
    AGREGAR_SUCURSAL, FORMULARIO_SUCURSAL, OBTENER_SUCURSALES, SUCURSAL_ACTUAL, ACTUALIZAR_SUCURSAL, ELIMINAR_SUCURSAL, SUCURSAL_ERROR
} from '../../types';
 
const SucursalState = props => {

    const initialState = {
        formulario: false,
        sucursales: [],
        sucursal: null,
        mensaje: null
    };

    //Dispatch para ejecutar acciones
    const [state, dispatch] = useReducer(sucursalReducer, initialState)

    //serie de funciones para CRUD
    const mostrarFormulario = () => {
        dispatch({
            type: FORMULARIO_SUCURSAL
        });
    };

    //Obtner los sucursales
    const obtenerSucursales = async () => {
        try {
            const resultado = await clienteAxios.get('/api/sucursales');

            dispatch({
                type: OBTENER_SUCURSALES,
                payload: resultado.data.sucursales
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: SUCURSAL_ERROR,
                payload: alerta
            });
        }
    };

    //Agregar sucursal
    const agregarSucursal = async sucursal => {
        try {
            const resultado = await clienteAxios.post('/api/sucursales', sucursal);
            dispatch({
                type: AGREGAR_SUCURSAL,
                payload: resultado.data
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: SUCURSAL_ERROR,
                payload: alerta
            });
        }
    };

    //selecciona el sucursal que el usuario da click
    const sucursalActual = sucursalID => {
        dispatch({
            type: SUCURSAL_ACTUAL,
            payload: sucursalID
        });
    };

    //edita o modifica la sucursal
    const actualizarSucursal = async sucursal => {
        try {
            const resultado = await clienteAxios.put(`/api/sucursales/${sucursal._id}`, sucursal);

            dispatch({
                type: ACTUALIZAR_SUCURSAL,
                payload: resultado.data.sucursal
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: SUCURSAL_ERROR,
                payload: alerta
            });
        }
    };

    //elimina sucursal
    const eliminarSucursal = async sucursalID => {
        try {
            await clienteAxios.delete(`/api/sucursales/${sucursalID}`);
            dispatch({
                type: ELIMINAR_SUCURSAL,
                payload: sucursalID
            });
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            };
            dispatch({
                type: SUCURSAL_ERROR,
                payload: alerta
            });
        }
    };

    return (
        <sucursalContext.Provider
            value={{
                formulario: state.formulario,
                sucursales: state.sucursales,
                sucursal: state.sucursal,
                mensaje: state.mensaje,
                mostrarFormulario,
                obtenerSucursales,
                agregarSucursal,
                sucursalActual,
                actualizarSucursal,
                eliminarSucursal
            }}
        >
            {props.children}
        </sucursalContext.Provider>
    )
}

export default SucursalState;
