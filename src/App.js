import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/header/Header';
import CrearCuenta from './components/login/CrearCuenta';
import Login from './components/login/Login';
import Principal from './components/principal/Principal';
import Footer from './components/footer/Footer';
import AuthState from './context/auth/authState';
import AlertaState from './context/alertas/alertaState';
import SucursalState from './context/sucursales/sucursalState';
import ProductoState from './context/productos/productoState';
import InventarioState from './context/inventarios/inventarioState';
import tokenAuth from './config/token';
import RutaPrivada from './config/RutaPrivada';
import './App.css';

//revisar si tenemos un token
const token = localStorage.getItem('token');

if(token) {
  tokenAuth(token);
}

const App = () => {
  return (
    <SucursalState>
      <ProductoState>
        <InventarioState>
          <AlertaState>
            <AuthState>
              <Router>
                <Header/>
                  <div className="container">
                    <Switch>
                        <Route exact path="/" component={Login}/>
                        <Route exact path="/crear-cuenta" component={CrearCuenta}/>
                        <RutaPrivada exact path="/principal" component={Principal}/>
                    </Switch>
                  </div>
                <Footer/>
              </Router>
            </AuthState>
          </AlertaState>
        </InventarioState>
      </ProductoState>
    </SucursalState>
  );
}
 
export default App;
